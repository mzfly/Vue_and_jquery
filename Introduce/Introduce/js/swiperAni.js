// export default {
function changeFunc(str,time) {
    var mySwiper = new Swiper(str, {
        onInit: function (swiper) { //Swiper2.x的初始化是onFirstInit

        },
        onSlideChangeEnd: function (swiper) {

        },
        // 翻转效果: flip, 
        effect: 'flip',
        flip: {
            slideShadows: true,
            limitRotation: true,
        },
        // 是否循环:
        loop: true,
        // 是否自动播放:
        autoplay: time
    })
}
// }